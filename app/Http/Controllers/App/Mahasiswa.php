<?php

namespace app\Http\Controllers\App;

use app\Models\mMember;
use app\Models\mPeserta;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\Session;

class Mahasiswa extends Controller
{
    private $breadcrumb;

    function __construct()
    {

    }

    function list()
    {
        $member = Session::get('member');
        $id_member = $member->id_member;
        $mahasiswa = mMember
            ::leftJoin('peserta', 'peserta.id_member', '=', 'member.id_member')
            ->where('mbr_status', 'mahasiswa')
            ->orderBy('pst_nama', 'ASC')
            ->get();

        $data = [
            'mahasiswa' => $mahasiswa,
            'navigation' => [
                'tab_active' => 'mahasiswa'
            ],
        ];

        return view('ujian/mahasiswa/mahasiswaList', $data);
    }

    function create()
    {

        $data = [
            'navigation' => [
                'tab_active' => 'mahasiswa'
            ],
        ];

        return view('ujian/mahasiswa/mahasiswaCreate', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'pst_nama' => 'required',
            'pst_phone' => 'required',
            'pst_alamat' => 'required',
        ]);

        $member = Session::get('member');
        $id_member = $member->id_member;
        $pst_nama = $request->input('pst_nama');
        $pst_phone = $request->input('pst_phone');
        $pst_alamat = $request->input('pst_alamat');
        $pst_keterangan = $request->input('pst_keterangan');

        $data_insert = [
            'id_member' => $id_member,
            'pst_nama' => $pst_nama,
            'pst_phone' => $pst_phone,
            'pst_alamat' => $pst_alamat,
            'pst_keterangan' => $pst_keterangan
        ];

        mPeserta::create($data_insert);
        return [
            'redirect' => route('mahasiswaList')
        ];
    }

    function edit($id_peserta)
    {
        $peserta = mPeserta::where('id_peserta', $id_peserta)->first();
        $data = [
            'peserta' => $peserta,
            'navigation' => [
                'tab_active' => 'mahasiswa'
            ],
        ];

        return view('ujian/mahasiswa/mahasiswaEdit', $data);
    }

    function update(Request $request, $id_peserta)
    {
        $request->validate([
            'pst_nama' => 'required',
            'pst_phone' => 'required',
            'pst_alamat' => 'required',
        ]);

        $pst_nama = $request->input('pst_nama');
        $pst_phone = $request->input('pst_phone');
        $pst_alamat = $request->input('pst_alamat');
        $pst_keterangan = $request->input('pst_keterangan');

        $data_update = [
            'pst_nama' => $pst_nama,
            'pst_phone' => $pst_phone,
            'pst_alamat' => $pst_alamat,
            'pst_keterangan' => $pst_keterangan
        ];

        mPeserta::where('id_peserta', $id_peserta)->update($data_update);

        return [
            'redirect' => route('mahasiswaList')
        ];
    }

    function delete($id_peserta)
    {
        mPeserta::where('id_peserta', $id_peserta)->delete();
    }
}
