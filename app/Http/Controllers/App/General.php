<?php

namespace app\Http\Controllers\App;

use app\Models\mMember;
use app\Models\mPeserta;
use app\Models\mUserRole;
use app\Rules\UjianEmailChecker;
use app\Rules\UjianEmailCheckerUpdate;
use app\Rules\UjianLoginCheck;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class General extends Controller
{
    private $breadcrumb;

    function __construct()
    {

    }

    function index()
    {
        $login_member = Session::get('login_member');
        return view('ujian/intro/intro');
    }

    function register_page()
    {
        return view('ujian/register/register');
    }

    function register_dosen_page()
    {
        return view('ujian/register/register_dosen');
    }

    function register_mahasiswa_page()
    {
        return view('ujian/register/register_mahasiswa');
    }

    function register_process(Request $request)
    {
        $request->validate([
            'mbr_nama' => 'required',
            'mbr_email' => [
                'required',
                'email',
                new UjianEmailChecker
            ],
            'mbr_password' => 'required',
            'mbr_tanggal_lahir' => 'required',
            'mbr_alamat' => 'required',
            'mbr_pekerjaan' => 'required',
        ]);

        $mbr_nama = $request->input('mbr_nama');
        $mbr_email = $request->input('mbr_email');
        $mbr_password = $request->input('mbr_password');
        $mbr_tanggal_lahir = $request->input('mbr_tanggal_lahir');
        $mbr_alamat = $request->input('mbr_alamat');
        $mbr_pekerjaan = $request->input('mbr_pekerjaan');

        $member_data = [
            'mbr_nama' => $mbr_nama,
            'mbr_email' => $mbr_email,
            'mbr_password' => Hash::make($mbr_password),
            'mbr_tanggal_lahir' => date('Y-m-d', strtotime($mbr_tanggal_lahir)),
            'mbr_alamat' => $mbr_alamat,
            'mbr_pekerjaan' => $mbr_pekerjaan
        ];

        mMember::create($member_data);
    }

    function register_dosen_process(Request $request)
    {
        $request->validate([
            'mbr_nama' => 'required',
            'mbr_nidn' => 'required',
            'mbr_email' => [
                'required',
                'email',
                new UjianEmailChecker
            ],
            'mbr_password' => 'required',
            'mbr_tanggal_lahir' => 'required',
            'mbr_alamat' => 'required',
            'mbr_phone' => 'required',
//            'mbr_pekerjaan' => 'required',
        ]);

        $mbr_nama = $request->input('mbr_nama');
        $mbr_nidn = $request->input('mbr_nidn');
        $mbr_email = $request->input('mbr_email');
        $mbr_password = $request->input('mbr_password');
        $mbr_tanggal_lahir = $request->input('mbr_tanggal_lahir');
        $mbr_alamat = $request->input('mbr_alamat');
        $mbr_phone = $request->input('mbr_phone');
        $mbr_pekerjaan = $request->input('mbr_pekerjaan');

        $member_data = [
            'mbr_nama' => $mbr_nama,
            'mbr_nidn' => $mbr_nidn,
            'mbr_email' => $mbr_email,
            'mbr_password' => Hash::make($mbr_password),
            'mbr_tanggal_lahir' => date('Y-m-d', strtotime($mbr_tanggal_lahir)),
            'mbr_alamat' => $mbr_alamat,
            'mbr_phone' => $mbr_phone,
//            'mbr_pekerjaan' => $mbr_pekerjaan,
            'mbr_status' => 'dosen'
        ];

        mMember::create($member_data);
    }

    function register_mahasiswa_process(Request $request)
    {
        $request->validate([
            'mbr_nama' => 'required',
            'mbr_nim' => 'required',
            'mbr_prodi' => 'required',
            'mbr_email' => [
                'required',
                'email',
                new UjianEmailChecker
            ],
            'mbr_password' => 'required',
            'mbr_tanggal_lahir' => 'required',
            'mbr_alamat' => 'required',
            'mbr_phone' => 'required',
//            'mbr_pekerjaan' => 'required',
        ]);

        $mbr_nama = $request->input('mbr_nama');
        $mbr_nim = $request->input('mbr_nim');
        $mbr_prodi = $request->input('mbr_prodi');
        $mbr_email = $request->input('mbr_email');
        $mbr_password = $request->input('mbr_password');
        $mbr_tanggal_lahir = $request->input('mbr_tanggal_lahir');
        $mbr_alamat = $request->input('mbr_alamat');
        $mbr_phone = $request->input('mbr_phone');
        $mbr_pekerjaan = $request->input('mbr_pekerjaan');

        $member_data = [
            'mbr_nama' => $mbr_nama,
            'mbr_nim' => $mbr_nim,
            'mbr_prodi' => $mbr_prodi,
            'mbr_email' => $mbr_email,
            'mbr_password' => Hash::make($mbr_password),
            'mbr_tanggal_lahir' => date('Y-m-d', strtotime($mbr_tanggal_lahir)),
            'mbr_alamat' => $mbr_alamat,
            'mbr_phone' => $mbr_phone,
//            'mbr_pekerjaan' => $mbr_pekerjaan,
            'mbr_status' => 'mahasiswa'
        ];

        $id_member = mMember::create($member_data)->id_member;

        $data_peserta = [
            'id_member' => $id_member,
            'pst_nama' => $mbr_nama,
            'pst_alamat' => $mbr_alamat,
            'pst_phone' => $mbr_phone
        ];

        mPeserta::create($data_peserta);

    }

    function login_page()
    {
        return view('ujian/login/login');
    }

    function login_process(Request $request)
    {
        $request->validate([
            'mbr_email' => [
                'required',
                'email'
            ],
            'mbr_password' => [
                'required',
                new UjianLoginCheck($request->input('mbr_email'))
            ],
        ]);

    }

    function logout_process()
    {
        Session::flush();
        return redirect()->route('ujianIntro');
    }

    function profile_edit()
    {
        $data = [
            'member' => Session::get('member'),
            'navigation' => [
                'tab_active' => 'user'
            ]
        ];
        return view('ujian/profile/profileEdit', $data);
    }

    function profile_update(Request $request) {
        $member = Session::get('member');
        $id_member = $member->id_member;

        $request->validate([
            'mbr_nama' => 'required',
            'mbr_email' => [
                'required',
                'email',
                new UjianEmailCheckerUpdate($id_member)
            ],
            'mbr_tanggal_lahir' => 'required',
            'mbr_alamat' => 'required',
            'mbr_pekerjaan' => 'required',
        ]);

        $mbr_nama = $request->input('mbr_nama');
        $mbr_email = $request->input('mbr_email');
        $mbr_password = $request->input('mbr_password');
        $mbr_tanggal_lahir = $request->input('mbr_tanggal_lahir');
        $mbr_alamat = $request->input('mbr_alamat');
        $mbr_pekerjaan = $request->input('mbr_pekerjaan');

        $member_data = [
            'mbr_nama' => $mbr_nama,
            'mbr_email' => $mbr_email,
            'mbr_tanggal_lahir' => date('Y-m-d', strtotime($mbr_tanggal_lahir)),
            'mbr_alamat' => $mbr_alamat,
            'mbr_pekerjaan' => $mbr_pekerjaan
        ];

        if($mbr_password) {
            $member_data['mbr_password'] = Hash::make($mbr_password);
        }

        mMember::where('id_member', $id_member)->update($member_data);
        $member = mMember::where('id_member', $id_member)->first();
        Session::put([
            'member' => $member
        ]);
    }

    function app_about()
    {
        $data = [
            'navigation' => [
                'tab_active' => 'user'
            ]
        ];
        return view('ujian/app/appAbout', $data);
    }
}
