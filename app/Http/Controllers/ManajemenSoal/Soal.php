<?php

namespace app\Http\Controllers\ManajemenSoal;

use app\Models\mBidang;
use app\Models\mBidangSub;
use app\Models\mSoal;
use app\Models\mSoalKategori;
use app\Models\mSoalSection;
use http\Client\Response;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

class Soal extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['manajemen_soal'],
                'route' => ''
            ],
            [
                'label' => $cons['soal'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $bidang = mBidang::orderBy('bdg_nama', 'ASC')->get();
        $id_bidang = isset($_GET['id_bidang']) ? $_GET['id_bidang'] : '';
        $id_bidang_sub = isset($_GET['id_bidang_sub']) ? $_GET['id_bidang_sub'] : '';

        if (isset($_GET['id_bidang'])) {
            $bidang_sub = mBidangSub::where('id_bidang', $id_bidang)->orderBy('bds_nama', 'ASC')->get();
        } else {
            $bidang_sub = [];
        }

        $soal_kategori = mSoalKategori
            ::with([
                'soal_section',
                'soal_section.soal'
            ])
            ->where('id_bidang_sub', $id_bidang_sub)
            ->get();


        $data = array_merge($data, [
            'bidang' => $bidang,
            'bidang_sub' => $bidang_sub,
            'soal_kategori' => $soal_kategori,
            'id_bidang' => $id_bidang,
            'id_bidang_sub' => $id_bidang_sub
        ]);

        return view('manajemen_soal/soal/soalPage', $data);
    }

    function bidang_sub(Request $request)
    {
        $id_bidang = $request->input('id_bidang');
        $bidang_sub = mBidangSub::where('id_bidang', $id_bidang)->orderBy('bds_nama', 'ASC')->get();

        $html = '<option value="">Pilih Perasat</option>';
        foreach ($bidang_sub as $row) {
            $html .= '<option value="' . $row->id_bidang_sub . '">' . $row->bds_nama . '</option>';
        }

        return $html;
    }

    function kategori_insert(Request $request)
    {
        $request->validate([
            'id_bidang' => 'required',
            'id_bidang_sub' => 'required',
            'skg_nilai' => 'required',
            'skg_tipe' => 'required',
            'skg_isi' => 'required',
        ]);

        $id_bidang = $request->input('id_bidang');
        $id_bidang_sub = $request->input('id_bidang_sub');
        $skg_nilai = $request->input('skg_nilai');
        $skg_tipe = $request->input('skg_tipe');
        $skg_isi = $request->input('skg_isi');

        $nilai_sum = mSoalKategori::where('id_bidang_sub', $id_bidang_sub)->sum('skg_nilai') + $skg_nilai;

        if ($nilai_sum > 10) {
            $message = 'Persentase Nilai sudah 10, sehingga tidak bisa menambah kembali';
            $status = FALSE;
        } else {
            $message = '';
            $status = TRUE;
        }

        if (!$status) {
            return response([
                'message' => $message,
                'errors' => [
                    'skg_nilai' => [
                        $message
                    ]
                ]
            ], 422);
        }


        $data_insert = [
            'id_bidang' => $id_bidang,
            'id_bidang_sub' => $id_bidang_sub,
            'skg_nilai' => $skg_nilai,
            'skg_tipe' => $skg_tipe,
            'skg_isi' => $skg_isi
        ];

        mSoalKategori::create($data_insert);
    }

    function kategori_edit_modal($id_soal_kategori)
    {
        $edit = mSoalKategori::where('id_soal_kategori', $id_soal_kategori)->first();

        $data = [
            'edit' => $edit
        ];

        return view('manajemen_soal/soal/soalKategoriEditModal', $data);
    }

    function kategori_update(Request $request)
    {
        $request->validate([
            'skg_nilai' => 'required',
            'skg_tipe' => 'required',
            'skg_isi' => 'required',
        ]);

        $id_soal_kategori = $request->input('id_soal_kategori');
        $skg_nilai = $request->input('skg_nilai');
        $skg_tipe = $request->input('skg_tipe');
        $skg_isi = $request->input('skg_isi');

        $soal_kategori = mSoalKategori::where('id_soal_kategori', $id_soal_kategori)->first();

        $nilai_sum = mSoalKategori
                ::where('id_bidang_sub', $soal_kategori->id_bidang_sub)
                ->where('id_soal_kategori', '!=', $id_soal_kategori)
                ->sum('skg_nilai') + $skg_nilai;

//        return $nilai_sum;

        if ($nilai_sum > 10) {
            $message = 'Persentase Nilai sudah 10, sehingga tidak bisa menambah kembali';
            $status = FALSE;
        } else {
            $message = '';
            $status = TRUE;
        }

        if (!$status) {
            return response([
                'message' => $message,
                'errors' => [
                    'skg_nilai' => [
                        $message
                    ]
                ]
            ], 422);
        }


        $data_update = [
            'id_soal_kategori' => $id_soal_kategori,
            'skg_nilai' => $skg_nilai,
            'skg_tipe' => $skg_tipe,
            'skg_isi' => $skg_isi
        ];

        mSoalKategori::where('id_soal_kategori', $id_soal_kategori)->update($data_update);

//        return redirect()->route('soalList', ['id_bidang' => $soal_kategori->id_bidang, 'id_bidang_sub' => $soal_kategori->id_bidang_sub]);
    }

    function kategori_delete($id_soal_kategori)
    {
        mSoalKategori::where('id_soal_kategori', $id_soal_kategori)->delete();
    }

    function sesi_insert(Request $request)
    {
        $request->validate([
            'sos_isi' => 'required'
        ]);

        $id_soal_kategori = $request->input('id_soal_kategori');
        $sos_isi = $request->input('sos_isi');

        $data_insert = [
            'id_soal_kategori' => $id_soal_kategori,
            'sos_isi' => $sos_isi
        ];

        mSoalSection::create($data_insert);

    }

    function sesi_edit_modal($id_soal_section)
    {
        $edit = mSoalSection::where('id_soal_section', $id_soal_section)->first();

        $data = [
            'edit' => $edit
        ];

        return view('manajemen_soal/soal/soalSectionEditModal', $data);
    }

    function sesi_update(Request $request)
    {
        $request->validate([
            'sos_isi' => 'required'
        ]);

        $id_soal_section = $request->input('id_soal_section');
        $sos_isi = $request->input('sos_isi');

        $data_update = [
            'sos_isi' => $sos_isi
        ];

        mSoalSection::where('id_soal_section', $id_soal_section)->update($data_update);

    }

    function sesi_delete($id_soal_section)
    {
        mSoalSection::where('id_soal_section', $id_soal_section)->delete();
    }

    function soal_insert(Request $request)
    {
        $request->validate([
            'sol_isi' => 'required'
        ]);

        $id_soal_section = $request->input('id_soal_section');
        $sol_isi = $request->input('sol_isi');

        $data_insert = [
            'id_soal_section' => $id_soal_section,
            'sol_isi' => $sol_isi
        ];

        mSoal::create($data_insert);

    }

    function soal_edit_modal($id_soal)
    {
        $edit = mSoal::where('id_soal', $id_soal)->first();

        $data = [
            'edit' => $edit
        ];

        return view('manajemen_soal/soal/soalEditModal', $data);
    }

    function soal_update(Request $request)
    {
        $request->validate([
            'sol_isi' => 'required'
        ]);

        $id_soal = $request->input('id_soal');
        $sol_isi = $request->input('sol_isi');

        $data_update = [
            'sol_isi' => $sol_isi
        ];

        mSoal::where('id_soal', $id_soal)->update($data_update);

    }

    function soal_delete($id_soal)
    {
        mSoal::where('id_soal', $id_soal)->delete();
    }
}
