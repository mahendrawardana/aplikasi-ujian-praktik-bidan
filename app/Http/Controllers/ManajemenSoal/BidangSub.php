<?php

namespace app\Http\Controllers\ManajemenSoal;

use app\Models\mBidang;
use app\Models\mBidangSub;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;

class BidangSub extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['manajemen_soal'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_bidang'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $bidang = mBidang::orderBy('bdg_nama', 'ASC')->get();
        $data_list = mBidangSub
            ::leftJoin('bidang', 'bidang.id_bidang', '=', 'bidang_sub.id_bidang')
            ->orderBy('bdg_nama', 'ASC')
            ->orderBy('bds_nama', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list,
            'bidang' => $bidang
        ]);

        return view('manajemen_soal/bidang_sub/bidangSubList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'id_bidang' => 'required',
            'bds_nama' => 'required'
        ]);

        $data = $request->except('_token');
        mBidangSub::create($data);
    }

    function edit_modal($id_bidang_sub)
    {
        $id_bidang_sub = Main::decrypt($id_bidang_sub);
        $edit = mBidangSub::where('id_bidang_sub', $id_bidang_sub)->first();
        $bidang = mBidang::orderBy('bdg_nama', 'ASC')->get();
        $data = [
            'edit' => $edit,
            'bidang' => $bidang
        ];

        return view('manajemen_soal/bidang_sub/bidangSubEditModal', $data);
    }

    function delete($id_bidang_sub)
    {
        $id_bidang_sub = Main::decrypt($id_bidang_sub);
        mBidangSub::where('id_bidang_sub', $id_bidang_sub)->delete();
    }

    function update(Request $request, $id_bidang_sub)
    {
        $id_bidang_sub = Main::decrypt($id_bidang_sub);
        $request->validate([
            'id_bidang' => 'required',
            'bds_nama' => 'required'
        ]);
        $data = $request->except("_token");
        mBidangSub::where(['id_bidang_sub' => $id_bidang_sub])->update($data);
    }
}
