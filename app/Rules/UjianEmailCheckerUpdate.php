<?php

namespace app\Rules;

use app\Models\mMember;
use Illuminate\Contracts\Validation\Rule;
use app\Models\mUser;

class UjianEmailCheckerUpdate implements Rule
{

    protected $id_member;
    protected $mbr_email;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id_member)
    {
        $this->id_member = $id_member;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $id_member = $this->id_member;
        $email_first = mMember::select('mbr_email')->where('id_member', $id_member)->value('mbr_email');
        $mbr_email = $value;
        $this->mbr_email = $value;

        if($mbr_email == $email_first) {
            return TRUE;
        } else {
            $checkSame = mMember::where('mbr_email', $mbr_email)->whereNotIn('mbr_email', array($email_first))->count();
            if($checkSame > 0) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Email <strong>'.$this->username.'</strong> Tidak Tersedia';
    }
}
