<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mSoalSection extends Model
{
    use SoftDeletes;

    protected $table = 'soal_section';
    protected $primaryKey = 'id_soal_section';
    protected $fillable = [
        'id_soal_kategori',
        'sos_isi'
    ];

    public function soal() {
        return $this->hasMany(mSoal::class, 'id_soal_section');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
