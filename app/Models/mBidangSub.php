<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mBidangSub extends Model
{
    use SoftDeletes;

    protected $table = 'bidang_sub';
    protected $primaryKey = 'id_bidang_sub';
    protected $fillable = [
        'id_bidang',
        'bds_nama',
        'bds_keterangan',
    ];

    public function bidang() {
        return $this->belongsTo(mBidang::class, 'id_bidang');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
