<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mSoal extends Model
{
    use SoftDeletes;

    protected $table = 'soal';
    protected $primaryKey = 'id_soal';
    protected $fillable = [
        'id_soal_section',
        'sol_isi'
    ];

    public function peserta_jawaban() {
        return $this->hasMany(mPesertaJawaban::class, 'id_soal');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
