<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPesertaJawaban extends Model
{
    use SoftDeletes;

    protected $table = 'peserta_jawaban';
    protected $primaryKey = 'id_peserta_jawaban';
    protected $fillable = [
        'id_ujian',
        'id_member',
        'id_peserta',
        'id_bidang',
        'id_bidang_sub',
        'id_soal',
        'id_soal_essay',
        'pjw_nilai',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
