<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mSoalKategori extends Model
{
    use SoftDeletes;

    protected $table = 'soal_kategori';
    protected $primaryKey = 'id_soal_kategori';
    protected $fillable = [
        'id_bidang',
        'id_bidang_sub',
        'skg_tipe',
        'skg_nilai',
        'skg_isi'
    ];

    public function soal_section() {
        return $this->hasMany(mSoalSection::class, 'id_soal_kategori');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
