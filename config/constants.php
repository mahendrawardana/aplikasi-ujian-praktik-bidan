<?php
/**
 * Created by PhpStorm.
 * User: mahendrawardana
 * Date: 07/02/19
 * Time: 13.36
 */

return [
    'acc_api_token' => 'c890cc0bd6562ed5b3a2a1105d6ddb9af9fdadab0c55216255bb0465d23c604aab845955b7b43787392c108f5a8cc82292435c99e7e93720145d89193cac06ba',
    'prefixProduksi' => 'MKK-',
    'ppnPersen' => 10,
    'decimalStep' => .01,
    'kodeHutangSupplier' => 'HS',
    'kodePreOrder' => 'PO',
    'kodeHutangLain' => 'HL',
    'kodePiutangPelanggan' => 'PP',
    'kodePiutangLain' => 'PL',
    'kodePenyesuaianStokBahan' => 'PSB',
    'kodePenyesuaianStokProduk' => 'PSP',
    'kodeJurnalUmum' => 'JU',
    'kodeAsset' => 'AT',
    'kodeKategoryAsset' => 'KAS',
    'bankType' => 'BCA',
    'bankRekening' => '4161877888',
    'bankAtasNama' => 'BAMBANG PRANOTO',
    'companyName' => 'Sistem Klinik',
    'companyAddress' => 'JL. Jendral A Yani, No. 04, Baler Bale Agung, Negara',
    'companyPhone' => '0818 0260 9624',
    'companyTelp' => '(0365) 41100',
    'companyEmail' => 'kutuskutusbali@gmail.com',
    'companyBendahara' => 'ARNIEL',
    'companyTuju' => 'Bapak Angga',
    'topMenu' => [
        'dashboard' => 'dashboard',

        'manajemen_soal' => 'manajemen_soal',
            'bidang' => 'bidang',
            'sub_bidang' => 'sub_bidang',
            'kategori_soal' => 'kategori_soal',
            'sesi_soal' => 'sesi_soal',
            'data_soal' => 'data_soal',
            'soal' => 'soal',

        'manajemen_peserta' => 'manajemen_peserta',
            'peserta' => 'peserta',
            'jawaban_peserta' => 'jawaban_peserta',
            'nilai_akhir_peserta' => 'nilai_akhir_peserta',

        'manajemen_ujian' => 'manajemen_ujian',
            'ujian' => 'ujian',
            'petunjuk_ujian' => 'petunjuk_ujian',

        'member' => 'member',


        'masterData' => 'master_data',
        'master_role_user' => 'role_user',
        'master_user' => 'user',
        'master_staf' => 'staf',
    ]
];
