@extends('ujian/general/index')

@section('body')
    <form action="{{ route('ujianLoginProcess') }}"
          method="post"
          class="m-form form-send  needs-validation"
          autocomplete="off"
          data-redirect="{{ route('ujianList') }}">
        {{ csrf_field() }}
        <div class="container">
            <div class="row">
                <div class="col-12 text-center intro-head">
                    <br/>
                    <img src="{{ asset('images/logo-mitra-ria-husada.jpeg') }}">
                    <br/>
                    <br/>
                    <h6>Selamat Datang</h6>
                    <h2>Penilaian Praktik Kebidanan</h2>
                </div>
                <div class="col-12 intro-body">
                    <br/>
                    <br/>
                    <br/>
                    <div class="mb-3 row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mbr_email">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="mbr_password">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-primary">Login</button>
                            <a href="{{ route('ujianIntro') }}" class="btn btn-warning">Kembali</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 text-center intro-footer">
                    <img src="{{ asset('images/logo-ujian-praktik-bidan-row.png') }}" class="img-responsive">
                </div>
            </div>
        </div>
    </form>
@endsection