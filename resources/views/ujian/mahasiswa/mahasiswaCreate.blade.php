@extends('ujian/general/index')

@section('body')

    <form class="form-send" action="{{ route('mahasiswaInsert') }}" method="post">
        {{ csrf_field() }}
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <br/>
                    <h2>Tambah Mahasiswa</h2>
                </div>
                <div class="col-12">
                    <br/>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label required">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" name="pst_nama" class="form-control">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Telepon/WA</label>
                        <div class="col-sm-10">
                            <input type="text" name="pst_phone" class="form-control">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <input type="text" name="pst_alamat" class="form-control">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Keterangan</label>
                        <div class="col-sm-10">
                            <input type="text" name="pst_keterangan" class="form-control">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <div class="col-sm-12 text-center">
                            <br/>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{ route('mahasiswaList') }}" class="btn btn-warning">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <br/>
    <br/>
    <br/>


    @include('ujian.component.navigation', $navigation)
@endsection