@extends('ujian/general/index')

@section('body')
    <div class="app-body">
        <div class="container-fluid">
            <div class="row row-cols-1 row-cols-md-2 g-4">
                @foreach($mahasiswa as $row)
                    <div class="col-sm-6">
                        <div class="card">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <strong>Nama : </strong>{{ $row->pst_nama }}
                                </li>
                                <li class="list-group-item">
                                    <strong>NIM :</strong> {{ $row->mbr_nim }}
                                </li>
                                <li class="list-group-item">
                                    <strong>Program Studi :</strong> {{ $row->mbr_prodi }}
                                </li>
                                <li class="list-group-item">
                                    <strong>Telepon/wa :</strong> {{ $row->pst_phone }}
                                </li>
{{--                                <li class="list-group-item">--}}
{{--                                    <strong>Alamat :</strong> {{ $row->pst_alamat }}--}}
{{--                                </li>--}}
{{--                                <li class="list-group-item text-center">--}}
{{--                                    <a href="{{ route('mahasiswaEdit', ['id_peserta'=>$row->id_peserta]) }}" class="btn btn-success">Edit</a>--}}
{{--                                    <button type="button" class="btn-hapus btn btn-danger"--}}
{{--                                            data-route="{{ route('mahasiswaDelete', ['id_peserta'=>$row->id_peserta]) }}">--}}
{{--                                        Hapus--}}
{{--                                    </button>--}}
{{--                                </li>--}}
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

{{--        <div class="float-right">--}}
{{--            <a class="btn btn-success btn-lg btn-ujian-create" href="{{ route('mahasiswaCreate') }}">--}}
{{--                <i class="fas fa-plus"></i>--}}
{{--            </a>--}}
{{--        </div>--}}

    </div>


    @include('ujian.component.navigation', $navigation)


@endsection