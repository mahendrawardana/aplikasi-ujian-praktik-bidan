<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
<style type="text/css">
    body {
        font-size: 11px;
    }

    h1 {
        font-size: 20px;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
    }

    h2 {
        font-size: 16px;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
    }

    h3 {
        font-size: 12px;
        font-weight: bold;
    }

    .div-2 {
        width: 50%;
        float: left;
        padding: 0 4px 0 0;
    }

    .div-3 {
        width: 33.3%;
        float: left;
        padding: 0 4px 0 0;
    }

    .div-3 tr td {
        vertical-align: top;
        padding-top: 0;
    }

    .label-1 {
        margin-top: -10px;
        margin-bottom: -14px;
        padding-left: 30%;
    }

    .label-1 label {
        padding-top: 12px;
    }

    .label-2 {
        margin-top: -5px;
        margin-bottom: -6px;
        padding-left: 30%;
    }

    .label-2 label {
        padding-top: 12px;
    }

    .table th, .table td {
        padding: 2px 4px;
    }

    .table th {
        text-align: center;
    }

    .table th.number, .table td.number {
        text-align: right;
    }

    .bold {
        font-weight: bold;
    }

    </style>

<h4 align="center">Ujian Praktik Bidan</h4>
<h6 align="center" style="margin: 0; padding: 0">Keterampilan : {{ $bidang->bdg_nama }}</h6>
<h6 align="center" style="margin: 0; padding: 0">Perasat : {{ $bidang_sub->bds_nama }}</h6>
<br/>

<h3 align="center">Daftar Mahasiswa</h3>
<table class="table">
    <thead>
    <tr>
        <th width="40">No</th>
        <th>Nama Mahasiswa</th>
    </tr>
    </thead>
    <tbody>
    @foreach($peserta as $key => $row)
        <tr>
            <td class="text-center">{{ ++$key }}</td>
            <td class="text-center">{{ $row->pst_nama }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<h3 align="center">Daftar Tilik</h3>

<table width="100%" border="1">
    <thead>
    <tr>
        <th class="text-center" width="40" rowspan="2">No</th>
        <th class="text-center" rowspan="2">Langkah Kerja</th>
        <th class="text-center" colspan="{{ count($peserta) }}" width="200">Mhs</th>
    </tr>
    <tr>
        @foreach($peserta as $key => $row)
            <th class="text-center" width="30">{{ ++$key }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($soal_kategori as $key_kategori => $row_kategori)
        @php
            $no = 1;
            $id_soal_section = [];
            foreach($row_kategori->soal_section as $row_section ) {
                $id_soal_section[] = $row_section->id_soal_section;
            }

            $jumlah_soal = \app\Models\mSoal::whereIn('id_soal_section', $id_soal_section)->count();
        @endphp
        <tr class="table-secondary">
            <th class="text-center">{{ Main::number_to_alphabet($key_kategori) }}</th>
            <td colspan="{{ count($peserta) + 1 }}">
                <strong>{{ $row_kategori->skg_isi }},
                    <small>Nilai tertinggi = {{ $jumlah_soal * 2 }}</small>
                </strong>
            </td>
        </tr>

        @foreach($row_kategori->soal_section as $key_section => $row_section)

            @if($row_section->sos_isi == '-')

                @foreach($row_section->soal as $row_soal)
                    <tr>
                        <td align="center">{{ $no++ }}.</td>
                        <td>
                            @if($row_kategori->skg_tipe == 'soal_tersedia')
                                {!! $row_soal->sol_isi !!}
                            @else
                                @php
                                    $id_soal_essay = app\Models\mPesertaJawaban
                                    ::where([
                                        'id_ujian' => $ujian->id_ujian,
                                        'id_soal' => $row_soal->id_soal
                                    ])
                                    ->value('id_soal_essay');

                                    echo \app\Models\mSoalEssay::where('id_soal_essay', $id_soal_essay)->value('sey_isi');
                                @endphp
                            @endif
                        </td>
                        @foreach($peserta as $key => $row_peserta)
                            <td align="center" width="30">
                                {{
                                    \app\Models\mPesertaJawaban
                                    ::where([
                                        'id_ujian' => $ujian->id_ujian,
                                        'id_peserta' => $row_peserta->id_peserta,
                                        'id_soal' => $row_soal->id_soal
                                    ])
                                    ->value('pjw_nilai')
                                }}
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            @else
                <tr>
                    <td></td>
                    <td><strong>{{ $row_section->sos_isi }}</strong></td>
                    @foreach($peserta as $key => $row)
                        <td></td>
                    @endforeach
                </tr>
                @foreach($row_section->soal as $row_soal)
                    <tr>
                        <td align="center"> {{ $no++ }}.</td>
                        <td>
                            @if($row_kategori->skg_tipe == 'soal_tersedia')
                                {!! $row_soal->sol_isi !!}
                            @else
                                {{--                 <textarea
                                                         class="form-control"
                                                         name="soal_essay[{{ $row_kategori->id_soal_kategori }}][{{ $row_section->id_soal_section }}][{{ $row_soal->id_soal }}]"
                                                         required></textarea>--}}
                                @php
                                    $id_soal_essay = app\Models\mPesertaJawaban
                                    ::where([
                                        'id_ujian' => $ujian->id_ujian,
                                        'id_soal' => $row_soal->id_soal
                                    ])
                                    ->value('id_soal_essay');

                                    echo \app\Models\mSoalEssay::where('id_soal_essay', $id_soal_essay)->value('sey_isi');
                                @endphp
                            @endif
                        </td>
                        @foreach($peserta as $key => $row_peserta)
                            <td align="center" width="30">
                                {{
                                    \app\Models\mPesertaJawaban
                                    ::where([
                                        'id_ujian' => $ujian->id_ujian,
                                        'id_peserta' => $row_peserta->id_peserta,
                                        'id_soal' => $row_soal->id_soal
                                    ])
                                    ->value('pjw_nilai')
                                }}
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            @endif


        @endforeach

    @endforeach

    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th class="text-center">Nilai Akhir</th>
            @foreach($peserta as $row)
                @php
                    $peserta_nilai_akhir = \app\Models\mPesertaNilaiAkhir
                    ::where([
                        'id_ujian' => $ujian->id_ujian,
                        'id_peserta' => $row->id_peserta
                    ])
                    ->first();
                @endphp
                <th class="text-center">
                    {{ $peserta_nilai_akhir->pna_nilai }}<br />
                    {{ $peserta_nilai_akhir->pna_label }}
                </th>
            @endforeach
        </tr>
    </tfoot>
</table>
<br/>
<table width="100%">
    <tr>
        <td valign="top" width="70%">
            <h3 style="padding: 0; margin: 0"><strong>Catatan : </strong></h3>
            {{ $ujian->ujn_catatan }}
        </td>
        <td width="30%">
            <strong>Penguji :</strong>
            <br />
            <br />
            <br />
            <br />
            {{ $member->mbr_nama }}
        </td>
    </tr>
</table>

