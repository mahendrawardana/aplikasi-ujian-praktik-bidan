@extends('ujian/general/index')

@section('body')
    <form action="{{ route('ujianInsert') }}"
          method="post"
          class="m-form form-send  needs-validation"
          autocomplete="off"
          data-alert-show="true"
          data-alert-field-message="true">
        {{ csrf_field() }}

        <div class="app-body">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h2>Ujian Baru</h2>
                    </div>
                    <div class="col-12 text-center">
                        <br/>
                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Pilih Keterampilan :</label>
                            <div class="col-sm-10 bidang">
                                <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modal-bidang">
                                    Browse & Pilih
                                </button>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Pilih Perasat :</label>
                            <div class="col-sm-10 bidang">
                                <button type="button" class="btn btn-success" data-bs-toggle="modal"
                                        data-bs-target="#modal-bidang-sub">Browse & Pilih
                                </button>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Mahasiswa :</label>
                            <div class="col-sm-10">
                                <button type="button" class="btn btn-success" data-bs-toggle="modal"
                                        data-bs-target="#modal-mahasiswa">Browse & Pilih
                                </button>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-sm-12 text-center">
                                <hr/>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="{{ route('ujianList') }}" class="btn btn-warning">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modal-mahasiswa" tabindex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Pilih Mahasiswa</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body modal-body-list-item">
                        @foreach($peserta as $key => $row)
                            <div class="row">
                                <label class="form-check-label col-10" for="mahasiswa-{{ $key }}">
                                    Nama : {{ $row->mbr_nama }}<br />
                                    NIM : {{ $row->mbr_nim }}<br />
                                    Prodi : {{ $row->mbr_prodi }}<br />
                                </label>
                                <div class="col-2">
                                    <input class="form-check-input" type="checkbox" name="id_peserta[]"
                                           value="{{ $row->id_peserta }}" id="mahasiswa-{{ $key }}">
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-bidang" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Pilih Keterampilan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body modal-body-list-item">
                        @foreach($bidang as $key => $row)

                            <div class="row">
                                <label class="form-check-label col-10" for="bidang-{{ $key }}">
                                    {{ $row->bdg_nama }}
                                </label>
                                <div class="col-2">
                                    <input class="form-check-input"
                                           name="id_bidang"
                                           type="radio"
                                           value="{{ $row->id_bidang }}"
                                           id="bidang-{{ $key }}"
                                            {{ $key == 0 ? 'checked':'' }}>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-bidang-sub" tabindex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Pilih Perasat</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body modal-body-list-item bidang-sub-list">
                        @foreach($bidang_sub as $key => $row)
                            <div class="row">
                                <label class="form-check-label col-10" for="bidang-sub-{{ $key }}">
                                    {{ $row->bds_nama }}
                                </label>
                                <div class="col-2">
                                    <input class="form-check-input"
                                           type="radio"
                                           name="id_bidang_sub"
                                           value="{{ $row->id_bidang_sub }}"
                                           id="bidang-sub-{{ $key }}"
                                            {{ $key == 0 ? 'checked':'' }}>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-petunjuk" tabindex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Pilih Petunjuk</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body modal-body-list-item">
                        <div class="row">
                            <label class="form-check-label col-10" for="petunjuk-1">
                                <i class="fa fa-info-circle"></i> Petunjuk 1
                            </label>
                            <div class="col-2">
                                <input class="form-check-input" type="radio" name="petunjuk" value="" id="petunjuk-1">
                            </div>
                        </div>
                        <div class="row">
                            <label class="form-check-label col-10" for="petunjuk-2">
                                <i class="fa fa-info-circle"></i> Petunjuk 2
                            </label>
                            <div class="col-2">
                                <input class="form-check-input" type="radio" name="petunjuk" value="" id="petunjuk-2">
                            </div>
                        </div>
                        <div class="row">
                            <label class="form-check-label col-10" for="petunjuk-3">
                                <i class="fa fa-info-circle"></i> Petunjuk 3
                            </label>
                            <div class="col-2">
                                <input class="form-check-input" type="radio" name="petunjuk" value="" id="petunjuk-3"
                                       checked>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    @include('ujian.component.navigation', $navigation)
@endsection