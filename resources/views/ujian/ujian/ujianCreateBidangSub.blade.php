@foreach($bidang_sub as $key => $row)
    <div class="row">
        <label class="form-check-label col-10" for="bidang-sub-{{ $key }}">
            {{ $row->bds_nama }}
        </label>
        <div class="col-2">
            <input class="form-check-input"
                   type="radio"
                   name="id_bidang_sub"
                   value="{{ $row->id_bidang_sub }}"
                   id="bidang-sub-{{ $key }}"
                    {{ $key == 0 ? 'checked':'' }}>
        </div>
    </div>
@endforeach