@extends('ujian/general/index')

@section('body')
    <div class="app-body">
        <div class="container-fluid">
            <div class="row row-cols-1 row-cols-md-2 g-4">
                @foreach($ujian as $row)

                    @php($id_peserta_arr = json_decode($row->id_peserta_json, TRUE))
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <p>Nama Keterampilan : {{ $row->bidang->bdg_nama }}</p>
                            <p>Nama Perasat : {{ $row->bidang_sub->bds_nama }}</p>
                            <a href="{{ route('ujianFinish', ['id_ujian' => Main::encrypt($row->id_ujian)]) }}" class="btn btn-primary btn-sm">Lihat Nilai</a>
                            <a href="{{ route('ujianDownload', ['id_ujian' => Main::encrypt($row->id_ujian)]) }}" class="btn btn-info btn-sm">Download Nilai</a>
                            <button type="button" data-route="{{ route('ujianDelete', ['id_ujian' => Main::encrypt($row->id_ujian)]) }}" class="btn btn-danger btn-hapus btn-sm">Hapus</button>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <i class="fa fa-users"></i> Jumlah Mahasiswa : {{ count($id_peserta_arr) }}
                            </li>
                        </ul>
                        <div class="card-footer">
                            <i class="fa fa-calendar"></i> <small class="text-muted">Tanggal Ujian : {{ $row->created_at }}</small>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        <div class="float-right">
            <a class="btn btn-success btn-lg btn-ujian-create" href="{{ route('ujianCreate') }}">
                <i class="fas fa-plus"></i>
            </a>
        </div>

    </div>


    @include('ujian.component.navigation', $navigation)


@endsection