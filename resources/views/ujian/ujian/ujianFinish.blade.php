@extends('ujian/general/index')

@section('body')
    <div class="app-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h4>Ujian Selesai</h4>
                    <h6>Nama Keterampilan : {{ $ujian->bidang->bdg_nama }}</h6>
                    <h6>Nama Perasat : {{ $ujian->bidang_sub->bds_nama }}</h6>
{{--                    <br/>
                    <a href="" class="btn btn-danger">
                        <i class="fa fa-file-pdf"></i> Download Rekap Nilai Ujian ke PDF
                    </a>--}}
                    <br />
                    <br />
                </div>
                <div class="col-sm-12">
                    @foreach($ujian->peserta_nilai_akhir as $row)
                    <div class="card">
                        <div class="card-body text-center">
                            <strong>{{ $row->peserta->pst_nama }}</strong>
                            <br />
                            <br />
                            <h1>{{ $row->pna_nilai }}</h1>
                            <br />
                            {!! \app\Helpers\Main::peserta_lulus_view($row->pna_nilai) !!}
                            <br />
                            <br />
                        </div>
                    </div>
                    <br />
                    @endforeach
                </div>
            </div>
        </div>

    </div>

    <div class="app-footer">
        <ul class="nav nav-pills nav-fill">
            <li class="nav-item">
                <a class="nav-link active" href="{{ route('ujianList') }}">
                    <i class="fa fa-check"></i> Kembali ke Daftar Ujian
                </a>
            </li>
        </ul>
    </div>



@endsection