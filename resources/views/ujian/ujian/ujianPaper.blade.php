@extends('ujian/general/index')

@section('css')

@endsection
@section('js')
@endsection

@section('body')

    <form action="{{ route('ujianPaperInsert') }}" method="post" class="form-send">
        {{ csrf_field() }}

        <input type="hidden" name="id_bidang" value="{{ $bidang->id_bidang }}">
        <input type="hidden" name="id_bidang_sub" value="{{ $bidang_sub->id_bidang_sub }}">
        <input type="hidden" name="id_ujian" value="{{ $ujian->id_ujian }}">
        @foreach($peserta as $row)
            <input type="hidden" name="id_peserta[]" value="{{ $row->id_peserta }}">
        @endforeach


        <div class="app-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 align="center">Ujian Praktik Bidan</h4>
                        <h6 align="center">Keterampilan : {{ $bidang->bdg_nama }}</h6>
                        <h6 align="center">Perasat : {{ $bidang_sub->bds_nama }}</h6>
                        <br/>
                    </div>
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title text-center">Petunjuk Penilaian</h5>
                            </div>
                        </div>
                        <div class="accordion accordion-flush" id="accordionFlushExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingOne">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#flush-collapseOne" aria-expanded="false"
                                            aria-controls="flush-collapseOne">
                                        Skor : 0
                                    </button>
                                </h2>
                                <div id="flush-collapseOne" class="accordion-collapse collapse"
                                     aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <strong>Penilaian Soft Skill dan Hard Skill :</strong><br/>
                                        Langkah Prosedur Tidak Dikerjakan Sama Sekali<br/>
                                        <strong>Penilaian Responsif :</strong><br/>
                                        Bila Tidak Mampu Menjawab
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#flush-collapseTwo" aria-expanded="false"
                                            aria-controls="flush-collapseTwo">
                                        Skor : 1
                                    </button>
                                </h2>
                                <div id="flush-collapseTwo" class="accordion-collapse collapse"
                                     aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <strong>Penilaian Soft Skill dan Hard Skill :</strong><br/>
                                        Langkah Prosedur Dikerjakan Tapi Kurang Tepat<br/>
                                        <strong>Penilaian Responsif :</strong><br/>
                                        Bila Menjawab Tetapi Kurang Tepat
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#flush-collapseThree" aria-expanded="false"
                                            aria-controls="flush-collapseThree">
                                        Skor : 2
                                    </button>
                                </h2>
                                <div id="flush-collapseThree" class="accordion-collapse collapse"
                                     aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <strong>Penilaian Soft Skill dan Hard Skill :</strong><br/>
                                        Langkah Prosedur Dikerjakan Dengan Tepat<br/>
                                        <strong>Penilaian Responsif :</strong><br/>
                                        Bila Menjawab dengan Tepat
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <Br/>
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title text-center">Daftar Mahasiswa</h5>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th width="40">No</th>
                                        <th>Nama Mahasiswa</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($peserta as $key => $row)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $row->pst_nama }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <Br/>
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title text-center">Daftar Tilik</h5>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 ujian-table">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col" class="text-center" width="40" rowspan="2">No</th>
                                <th scope="col" class="text-center" rowspan="2">Langkah Kerja</th>
                                <th scope="col" class="text-center" colspan="{{ count($peserta) }}" width="200">Mhs</th>
                            </tr>
                            <tr>
                                @foreach($peserta as $key => $row)
                                    <th class="text-center">{{ ++$key }}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($soal_kategori as $key_kategori => $row_kategori)
                                @php
                                    $no = 1;
                                    $id_soal_section = [];
                                    foreach($row_kategori->soal_section as $row_section ) {
                                        $id_soal_section[] = $row_section->id_soal_section;
                                    }

                                    $jumlah_soal = \app\Models\mSoal::whereIn('id_soal_section', $id_soal_section)->count();
                                @endphp
                                <tr class="table-secondary">
                                    <th scope="row"
                                        class="text-center">{{ Main::number_to_alphabet($key_kategori) }}</th>
                                    <td colspan="{{ count($peserta) + 1 }}">
                                        <strong>{{ $row_kategori->skg_isi }},
                                            <small>Nilai tertinggi = {{ $jumlah_soal * 2 }}</small>
                                        </strong>
                                    </td>
                                </tr>

                                @foreach($row_kategori->soal_section as $key_section => $row_section)

                                    @if($row_section->sos_isi == '-')

                                        @foreach($row_section->soal as $row_soal)
                                            <tr>
                                                <td scope="row" class="text-center">{{ $no++ }}.</td>
                                                <td>
                                                    @if($row_kategori->skg_tipe == 'soal_tersedia')
                                                        {!! $row_soal->sol_isi !!}
                                                    @else
                                                        <textarea
                                                                class="form-control"
                                                                name="soal_essay[{{ $row_kategori->id_soal_kategori }}][{{ $row_section->id_soal_section }}][{{ $row_soal->id_soal }}]"
                                                                required></textarea>
                                                    @endif
                                                </td>
                                                @foreach($peserta as $key => $row_peserta)
                                                    <td>
                                                        <select class="form-control"
                                                                required
                                                                name="jawaban[{{ $row_kategori->id_soal_kategori }}][{{ $row_section->id_soal_section }}][{{ $row_soal->id_soal }}][{{ $row_peserta->id_peserta }}]">
                                                            <option value="0">0</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
{{--                                                            <option value="1" {{ $row_peserta->id_peserta == 1 ? 'selected':'' }}>1</option>--}}
{{--                                                            <option value="2" {{ $row_peserta->id_peserta == 2 ? 'selected':'' }}>2</option>--}}
                                                        </select>
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td scope="row" class="text-center"></td>
                                            <td><strong>{{ $row_section->sos_isi }}</strong></td>
                                            @foreach($peserta as $key => $row)
                                                <td></td>
                                            @endforeach
                                        </tr>
                                        @foreach($row_section->soal as $row_soal)
                                            <tr>
                                                <td scope="row" class="text-center">{{ $no++ }}.</td>
                                                <td>
                                                    @if($row_kategori->skg_tipe == 'soal_tersedia')
                                                        {!! $row_soal->sol_isi !!}
                                                    @else
                                                        <textarea
                                                                class="form-control"
                                                                name="soal_essay[{{ $row_kategori->id_soal_kategori }}][{{ $row_section->id_soal_section }}][{{ $row_soal->id_soal }}]"
                                                                required></textarea>
                                                    @endif
                                                </td>
                                                @foreach($peserta as $key => $row_peserta)
                                                    <td>
                                                        <select class="form-control"
                                                                required
                                                                name="jawaban[{{ $row_kategori->id_soal_kategori }}][{{ $row_section->id_soal_section }}][{{ $row_soal->id_soal }}][{{ $row_peserta->id_peserta }}]">
                                                            <option value="0">0</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
{{--                                                            <option value="1" {{ $row_peserta->id_peserta == 1 ? 'selected':'' }}>1</option>--}}
{{--                                                            <option value="2" {{ $row_peserta->id_peserta == 2 ? 'selected':'' }}>2</option>--}}
                                                        </select>
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    @endif


                                @endforeach

                            @endforeach

                            </tbody>
                            <tfoot>
                            <tr class="table-secondary">
                                <th scope="row" class="text-center"></th>
                                <td colspan="3">
                                    {{ $formula_nilai }}
                                </td>
                            </tr>
                            </tfoot>
                        </table>

                    </div>

                    <div class="col-sm-12">
                        <Br/>
                        <div class="card">
                            <div class="card-body">
                                <div class="mb-3">
                                    <label for="exampleFormControlTextarea1" class="form-label">Catatan :</label>
                                    <textarea class="form-control" name="ujn_catatan" id="exampleFormControlTextarea1" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="app-footer">
            <ul class="nav nav-pills nav-fill">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('ujianList') }}">Kembali</a>
                </li>
                <li class="nav-item">
                    <button type="submit" class="nav-link active">
                        <i class="fa fa-check"></i> Ujian Selesai
                    </button>
                </li>
            </ul>
        </div>

    </form>

@endsection