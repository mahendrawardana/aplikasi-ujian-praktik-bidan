@extends('ujian/general/index')

@section('body')
<div class="container">
    <div class="row">
        <div class="col-12 text-center intro-head">
            <br />
            <img src="{{ asset('images/logo-mitra-ria-husada.jpeg') }}">
            <br />
            <br />
            <h6>Selamat Datang</h6>
            <h2>Penilaian Praktik Kebidanan</h2>
        </div>
        <div class="col-12 text-center intro-body">
            <br />
            <br />
            <br />
            <br />
            <br />
            <a href="{{ route('ujianLogin') }}" class="btn btn-primary btn-lg">Login</a>
            <Br />
            <Br />
            atau
            <Br />
            <Br />
            <a href="{{ route('ujianRegisterDosen') }}" class="btn btn-primary btn-lg">Register Dosen</a>
            <Br />
            <Br />
            <a href="{{ route('ujianRegisterMahasiswa') }}" class="btn btn-primary btn-lg">Register Mahasiswa</a>
            <br />
            <br /><Br />
        </div>
        <div class="col-12 text-center intro-footer">
            <img src="{{ asset('images/logo-ujian-praktik-bidan-row.png') }}" class="img-responsive">
        </div>
    </div>
</div>
@endsection