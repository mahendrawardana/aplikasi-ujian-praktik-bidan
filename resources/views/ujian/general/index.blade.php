<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{ asset('bootstrap_5/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('bootstrap_5/css/bootstrap-grid.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/ujian.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('fontawesome/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/loading.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />

    @yield('css')

    <title>Ujian Praktik Bidan</title>
</head>
<body
   data-base-url="{{ url('/') }}"
   data-route-ujian-create-bidang-sub="{{ route('ujianCreateBidangSub') }}"
   data-token="{{ csrf_token() }}">

@yield('body')

<div class='container-loading d-none'>
    <div class='loader'>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--text'></div>
        <div class='loader--desc'></div>
    </div>
</div>

<script src="{{ asset('bootstrap_5/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery-3.6.0.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugin/sweetalert/sweetalert2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/ujian.js') }}" type="text/javascript"></script>

@yield('js')
</body>
</html>