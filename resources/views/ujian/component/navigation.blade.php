<div class="app-footer">
    <ul class="nav nav-pills nav-fill">
        <li class="nav-item">
            <a class="nav-link {{ $tab_active == 'ujian' ? ' active':'' }}" href="{{ route('ujianList') }}">Ujian</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ $tab_active == 'mahasiswa' ? ' active':'' }}" href="{{ route('mahasiswaList') }}">Mahasiswa</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ $tab_active == 'user' ? ' active':'' }}" href="#" data-bs-toggle="modal" data-bs-target="#modal-user">User</a>
        </li>
    </ul>
</div>

<div class="modal fade" id="modal-user"  data-bs-backdrop="static" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">
                    <i class="fa fa-user-alt"></i> {{ \Illuminate\Support\Facades\Session::get('member')['mbr_nama'] }}
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="padding: 0">
                <div class="list-group list-group-flush">
                    <a href="{{ route('profileEdit') }}" class="list-group-item">
                        <i class="fa fa-user"></i> Profile
                    </a>
                    <a href="{{ route('appAbout') }}" class="list-group-item">
                        <i class="fa fa-info-circle"></i> Tentang Aplikasi
                    </a>
                    <a href="{{ route('ujianLogoutProcess') }}" class="list-group-item">
                        <i class="fa fa-sign-out-alt"></i> Logout
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>