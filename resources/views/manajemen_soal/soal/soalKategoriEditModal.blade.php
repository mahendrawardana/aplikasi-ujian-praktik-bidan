<div class="modal" id="soal-modal-general" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <form action="{{ route('soalKategoriUpdate') }}"
                  method="post"
                  class="m-form form-send">
                {{ csrf_field() }}

                <input type="hidden" name="id_soal_kategori" value="{{ $edit->id_soal_kategori }}">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Persentase Nilai
                            </label>
                            <select class="form-control m-input" name="skg_nilai">
                                @for($i=1; $i<=10; $i++)
                                    <option value="{{ $i }}" {{ $i == $edit->skg_nilai ? 'selected':'' }}>{{ $i }}
                                    </option>
                                @endfor
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Tipe Soal
                            </label>
                            <select class="form-control m-input" name="skg_tipe">
                                <option value="soal_tersedia" {{ $edit->skg_tipe == 'soal_tersedia' ? 'selected':'' }}>
                                    Langkah Kerja Tersedia
                                </option>
                                <option value="diisi_penguji" {{ $edit->skg_tipe == 'diisi_penguji' ? 'selected':'' }}>
                                    Langkah Kerja Diisi Penguji
                                </option>
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Isi Kategori Soal
                            </label>
                            <input type="text" class="form-control m-input" name="skg_isi" value="{{ $edit->skg_isi }}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>

            </form>


        </div>
    </div>
</div>
