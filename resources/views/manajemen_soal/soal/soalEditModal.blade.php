<div class="modal" id="soal-modal-general" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <form action="{{ route('soalUpdate') }}"
                  method="post"
                  class="m-form form-send">
                {{ csrf_field() }}

                <input type="hidden" name="id_soal" value="{{ $edit->id_soal }}">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Isi Soal
                            </label>
                            <textarea name="sol_isi" class="form-control tinymce" data-provide="markdown" rows="10">
                                {{ $edit->sol_isi }}
                            </textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>

            </form>


        </div>
    </div>
</div>
