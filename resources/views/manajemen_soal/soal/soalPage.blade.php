@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')
    @include('manajemen_soal.bidang_sub.bidangSubCreate')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>

                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body form-filter">

                    <div class="form-group m-form__group">
                        <label class="form-control-label required">Nama Keterampilan</label>
                        <select class="form-control" name="id_bidang">
                            <option value="">Pilih Keterampilan</option>
                            @foreach($bidang as $row)
                                <option value="{{ $row->id_bidang }}" {{ $row->id_bidang == $id_bidang ? 'selected':'' }}>{{ $row->bdg_nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group m-form__group">
                        <label class="form-control-label required">Nama Perasat</label>
                        <select class="form-control" name="id_bidang_sub">
                            @foreach($bidang_sub as $row)
                                <option value="{{ $row->id_bidang_sub }}" {{ $row->id_bidang_sub == $id_bidang_sub ? 'selected':'' }}>{{ $row->bds_nama }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
            </div>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table m-table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th class="text-center" colspan="3">LANGKAH KERJA</th>
                            <th class="text-center" width="130">MENU</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($soal_kategori as $key => $row)
                            <tr class="m-table__row--success">
                                <td width="30">{{ Main::number_to_alphabet($key) }}</td>
                                <td colspan="3">{{ $row->skg_isi }}</td>
                                <td>
                                    <button
                                        type="button"
                                        class="btn btn-sm btn-success btn-modal-general-soal"
                                        data-route="{{ route('soalKategoriEdit', ['id_kategori' => $row->id_soal_kategori]) }}">
                                        Edit
                                    </button>
                                    <button
                                            type="button"
                                            class="btn btn-sm  btn-danger btn-soal-hapus"
                                            data-route="{{ route('soalKategoriDelete', ['id_soal_kategori' => $row->id_soal_kategori]) }}">
                                        Hapus
                                    </button>
                                </td>
                            </tr>
                            @foreach($row->soal_section as $key_2 => $row_2)
                            <tr class="m-table__row--warning">
                                <td width="30"></td>
                                <td width="30"></td>
                                <td colspan="2">{{ $row_2->sos_isi }}</td>
                                <td>
                                    <button
                                            type="button"
                                            class="btn btn-sm btn-success btn-modal-general-soal"
                                            data-route="{{ route('soalSectionEdit', ['id_soal_section' => $row_2->id_soal_section]) }}">
                                        Edit
                                    </button>
                                    <button
                                            type="button"
                                            class="btn btn-sm  btn-danger btn-soal-hapus"
                                            data-route="{{ route('soalSectionDelete', ['id_soal_section' => $row_2->id_soal_section]) }}">
                                        Hapus
                                    </button>
                                </td>
                            </tr>
                            @foreach($row_2->soal as $key_3 => $row_3)
                            <tr class="m-table__row--info">
                                <td></td>
                                <td></td>
                                <td width="30"></td>
                                <td>{!! $row_3->sol_isi  !!}</td>
                                <td>
                                    <button
                                            type="button"
                                            class="btn btn-sm btn-success btn-modal-general-soal"
                                            data-route="{{ route('soalEdit', ['id_soal' => $row_3->id_soal]) }}">
                                        Edit
                                    </button>
                                    <button
                                            type="button"
                                            class="btn btn-sm  btn-danger btn-soal-hapus"
                                            data-route="{{ route('soalDelete', ['id_soal' => $row_3->id_soal]) }}">
                                        Hapus
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button class="btn btn-sm  btn-info btn-soal-tambah" data-id-soal-section="{{ $row_2->id_soal_section }}">
                                        <i class="fa fa-plus"></i> Tambah Soal
                                    </button>
                                </td>
                                <td></td>
                            </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td colspan="2">
                                    <button
                                        class="btn btn-sm btn-warning btn-soal-tambah-kategori-sub"
                                        data-id-soal-kategori="{{ $row->id_soal_kategori }}"
                                        data-skg-isi="{{ $row->skg_isi }}">
                                        <i class="fa fa-plus"></i> Tambah Sesi Soal
                                    </button>
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4">
                                <button class="btn btn-sm btn-success" data-toggle="modal"
                                        data-target="#modal-kategori">
                                    <i class="fa fa-plus"></i> Tambah Kategori
                                </button>
                            </td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <form action="{{ route('soalKategoriInsert') }}" method="post"
          class="form-send m-form m-form--fit m-form--label-align-right">

        {{ csrf_field() }}
        <input type="hidden" name="id_bidang" value="{{ $id_bidang }}">
        <input type="hidden" name="id_bidang_sub" value="{{ $id_bidang_sub }}">

        <div class="modal" id="modal-kategori" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header row">
                        <div class="col-8">
                            <h4 class="modal-title" id="myModalLabel" style="width: 100%">Tambah Kategori</h4>
                        </div>
                        <div class="col-4">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                    style="float: right"><span aria-hidden="true">&times;</span></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Persentase Nilai
                            </label>
                            <select class="form-control m-input" name="skg_nilai">
                                @for($i=1; $i<=10; $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Tipe Soal
                            </label>
                            <select class="form-control m-input" name="skg_tipe">
                                <option value="soal_tersedia" selected>Langkah Kerja Tersedia</option>
                                <option value="diisi_penguji">Langkah Kerja Diisi Penguji</option>
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Isi Kategori Soal
                            </label>
                            <input type="text" class="form-control m-input" name="skg_isi">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm  btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-sm  btn-primary">Simpan</button>
                    </div>
                </div>
            </div>
        </div>

    </form>

    <form action="{{ route('soalSectionInsert') }}" method="post"
          class="form-send m-form m-form--fit m-form--label-align-right">

        {{ csrf_field() }}

        <div class="modal" id="modal-sesi-tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

            <input type="hidden" name="id_soal_kategori">

            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header row">
                        <div class="col-8">
                            <h4 class="modal-title" id="myModalLabel" style="width: 100%">Tambah Sesi</h4>
                        </div>
                        <div class="col-4">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                    style="float: right"><span aria-hidden="true">&times;</span></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1" class="font-weight-bold">
                                Kategori Soal
                            </label>
                            <br />
                            <span class="soal-sesi-kategori"></span>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Isi Sesi Soal
                            </label>
                            <input type="text" class="form-control m-input" name="sos_isi">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm  btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-sm  btn-primary">Simpan</button>
                    </div>
                </div>
            </div>
        </div>

    </form>

    <form action="{{ route('soalInsert') }}" method="post"
          class="form-send m-form m-form--fit m-form--label-align-right">

        {{ csrf_field() }}

        <div class="modal" id="modal-soal-tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

            <input type="hidden" name="id_soal_section">

            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header row">
                        <div class="col-8">
                            <h4 class="modal-title" id="myModalLabel" style="width: 100%">Tambah Soal</h4>
                        </div>
                        <div class="col-4">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                    style="float: right"><span aria-hidden="true">&times;</span></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Isi Soal
                            </label>
                            <textarea name="sol_isi" class="form-control tinymce" rows="10"></textarea>
                            <div class="summernote" id="m_summernote" name="sol_isi"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm  btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-sm  btn-primary">Simpan</button>
                    </div>
                </div>
            </div>
        </div>

    </form>

@endsection
