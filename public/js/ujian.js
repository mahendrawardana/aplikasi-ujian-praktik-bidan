$(document).ready(function () {

    form_send_wrapper();

    id_bidang_change();

    data_hapus();

});

function id_bidang_change() {
    $('[name="id_bidang"]').click(function () {
        var id_bidang = $(this).val();
        var route_bidang_sub = $('body').data('route-ujian-create-bidang-sub');
        var _token = $('body').data('token');
        var data_send = {
            id_bidang: id_bidang,
            _token: _token
        };

        $.ajax({
            url: route_bidang_sub,
            type: 'post',
            data: data_send,
            success: function (view) {
                $('.bidang-sub-list').html(view);
            }
        });
    });
}

function form_send_wrapper() {

    $('.form-send').submit(function (e) {
        e.preventDefault();

        var self = $(this);
        var val = false;
        var label_button = $(this).find('[type="submit"]').text();
        $(this).find('[type="submit"]').text('Loading ...').prop('disabled', true);


        var confirm_status = $(this).data('confirm-status');
        var confirm_message = $(this).data('confirm-message');

        if (confirm_status) {
            swal({
                title: "Perhatian ...",
                html: confirm_message,
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {
                    form_send(self, label_button);
                } else {
                    self.find('[type="submit"]').text(label_button).prop('disabled', false);
                }
            });
        } else {
            form_send(self, label_button);
        }
        return false;
    });
}

function form_send(self, label_button = '') {
    loading_start();
    var action = self.attr('action');
    var method = self.attr('method');

    var redirect = self.data('redirect');
    var pdf = self.data('pdf');
    var alert_show = self.data('alert-show');
    var alert_field_message = self.data('alert-field-message');

    var alert_show_success_status = self.data('alert-show-success-status');
    var alert_show_success_title = self.data('alert-show-success-title');
    var alert_show_success_message = self.data('alert-show-success-message');

    var message = '';
    var message_field = '';

    var form = self;
    var formData = new FormData(form[0]);

    $.ajax({
        url: action,
        type: method,
        data: formData,
        // async: false,
        beforeSend: function () {
            loading_start();
        },
        error: function (request, error) {
            loading_finish();
            $('.invalid-feedback').remove();
            $('input').removeClass('is-invalid');
            $.each(request.responseJSON.errors, function (key, val) {
                var type = $('[name="' + key + '"]').attr('type');
                message_field += val[0] + '<br />';
                $('[name="' + key + '"]').after('<div class="invalid-feedback">' + val[0] + '</div>');
                $('[name="' + key + '"]').addClass('is-invalid');
            });

            if (alert_show == true) {

                if (alert_field_message == true) {

                    if (message_field) {
                        message = message_field;
                    } else {
                        message = request.responseJSON.message;
                    }
                } else {
                    message = request.responseJSON.message;
                }
                Swal.fire({
                    title: 'Ada yang Salah',
                    html: message,
                    icon: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Hapus'
                });

            }


            $('.form-send [type="submit"]').text(label_button).prop('disabled', false);
        },
        success: function (data) {
            loading_finish();
            $('.form-send [type="submit"]').text(label_button).prop('disabled', false);
            if (alert_show_success_status) {
                swal({
                    title: alert_show_success_title,
                    html: alert_show_success_message,
                    type: 'success',
                    showDenyButton: true,
                    confirmButtonText: 'Baik',
                }).then(function (result) {
                    window.location.reload();
                });

            } else {

                if (data.redirect) {
                    window.location.href = data.redirect;
                    return false;
                }


                if (typeof redirect == 'undefined') {
                    window.location.reload();
                    form_clear();
                } else {
                    if (pdf) {
                        window.open(pdf, "_blank");
                        window.location.href = redirect;
                    } else {
                        window.location.href = redirect;
                    }
                }
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
}


function data_hapus() {

    $('.btn-hapus').click(function (e) {
        e.preventDefault();
        var self = $(this);
        var csrf_token = $('body').data('token');
        message = "Yakin hapus data ini ?";

        Swal.fire({
            title: 'Perhatian',
            text: message,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus'
        }).then(function(result) {
            if (result.isConfirmed) {
                $.ajax({
                    url: self.data('route'),
                    type: "get",
                    success: function () {
                        window.location.reload();
                    },
                    error: function (request) {
                        var title = request.responseJSON.title ? request.responseJSON.title : 'Ada yang salah';
                        swal({
                            title: title,
                            html: request.responseJSON.message,
                            type: "warning"
                        });
                    }
                });
            }
        });


        return false;
    });
}

function loading_start() {
    $('.container-loading').hide().removeClass('hidden').fadeIn('fast');
}

function loading_finish() {
    $('.container-loading').fadeOut('fast').addClass('hidden');
}