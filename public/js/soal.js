$(document).ready(function () {


    soal_bidang_change();
    soal_bidang_sub_change();
    bidang_change();

    btn_modal_general();

    data_hapus();

    soal_sesi_tambah();
    soal_tambah();

    tinymce_run();

});

function soal_bidang_change() {
    $('[name="id_bidang"]').change(function () {
        var id_bidang = $(this).val();
        var route_soal_bidang_sub = $('#base-value').data('route-soal-bidang-sub');
        var _token = $('#base-value').data('csrf-token');
        var data_send = {
            id_bidang: id_bidang,
            _token: _token
        };

        $.ajax({
            url: route_soal_bidang_sub,
            type: 'post',
            data: data_send,
            success: function (data) {
                $('[name="id_bidang_sub"]').html(data);
            }
        })
    });
}

function soal_bidang_sub_change() {
    $('[name="id_bidang_sub"]').change(function () {
        var id_bidang = $('.form-filter [name="id_bidang"]').val();
        var id_bidang_sub = $('.form-filter [name="id_bidang_sub"]').val();
        window.location.href = '?id_bidang=' + id_bidang + '&id_bidang_sub=' + id_bidang_sub;
    });
}

function bidang_change() {
    $('[name="id_bidang"]').change(function () {
        var id_bidang = $(this).val();
        $('[name="id_bidang"]').val(id_bidang);
    });

    $('[name="id_bidang_sub"]').change(function () {
        var id_bidang_sub = $(this).val();
        $('[name="id_bidang_sub"]').val(id_bidang_sub);
    });
}

function btn_modal_general() {
    $('.btn-modal-general-soal').click(function (e) {
        e.preventDefault();

        var self = $(this);
        var route = $(this).data('route');

        $.ajax({
            url: route,
            type: 'get',
            data: {},
            beforeSend: loading_start(),
            success: function (json) {
                loading_finish();
                $('.wrapper-modal').html(json);
                $('#soal-modal-general').modal('show');

                tinymce_run();
                form_send_wrapper();
            }
        });
        return false;
    });
}

function tinymce_run (){
    if ($('.tinymce').length > 0) {

        var base_url = $('#base-value').data('base-url');

        tinymce.init({
            selector: '.tinymce',
            height: 300,
            theme: 'modern',
            relative_urls: false,
            remove_script_host: false,
            convert_urls: false,
            entity_encoding : "raw",
            editor_encoding: "raw",
            apply_source_formatting:true,
            entities:'169,copy,8482,trade,ndash,8212,mdash,8216,lsquo,8217,rsquo,8220,ldquo,8221,rdquo,8364,euro',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools filemanager responsivefilemanager'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true,
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ],
            content_css: [
                '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                '//www.tinymce.com/css/codepen.min.css'
            ],

            external_filemanager_path: base_url + "/plugin/tinymce/plugins/filemanager/",
            filemanager_title: "Responsive Filemanager",
            external_plugins: {"filemanager": base_url + "/plugin/tinymce/plugins/filemanager/plugin.min.js"}

        });


    }
}

function data_hapus() {

    $('.btn-soal-hapus').click(function (e) {
        e.preventDefault();
        var self = $(this);
        var csrf_token = $('#base-value').data('csrf-token');
        if (typeof message === "undefined") {
            message = "Yakin hapus data ini ?";
        }
        swal({
            title: "Perhatian ...",
            html: message,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Batal",
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    url: self.data('route'),
                    type: "get",
                    success: function () {
                        window.location.reload();
                    },
                    error: function (request) {
                        var title = request.responseJSON.title ? request.responseJSON.title : 'Ada yang salah';
                        swal({
                            title: title,
                            html: request.responseJSON.message,
                            type: "warning"
                        });
                    }
                });
            }
        });
        return false;
    });
}

function soal_sesi_tambah() {
    $('.btn-soal-tambah-kategori-sub').click(function () {
        var id_soal_kategori = $(this).data('id-soal-kategori');
        var skg_isi = $(this).data('skg-isi');

        $('.soal-sesi-kategori').html(skg_isi);
        $('#modal-sesi-tambah [name="id_soal_kategori"]').val(id_soal_kategori);

        $('#modal-sesi-tambah').modal('show');
    });
}

function soal_tambah() {
    $('.btn-soal-tambah').click(function () {
        var id_soal_section = $(this).data('id-soal-section');

        $('#modal-soal-tambah [name="id_soal_section"]').val(id_soal_section);

        $('#modal-soal-tambah').modal('show');
    });
}