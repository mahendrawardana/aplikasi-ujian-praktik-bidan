<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;


Route::get('/error-test', function () {
    Log::info('calling the error route');
    throw new \Exception('something unexpected broke');
});

Route::get('/maintenance', function () {
    Artisan::call('down');
});

Route::get('/production', function () {
    Artisan::call('up');
});

Route::post('/submit-form', function () {
    //
})->middleware(\Spatie\HttpLogger\Middlewares\HttpLogger::class);


Route::group(['namespace' => 'App'], function () {

    Route::get('/about', "General@app_about")->name('appAbout');

    Route::group(['middleware' => 'checkLoginUjian'], function () {
        Route::get('/', "General@index")->name('ujianIntro');
        Route::get('/register', "General@register_page")->name('ujianRegister');
        Route::post('/register', "General@register_process")->name('ujianRegisterProcess');

        Route::get('/register/dosen', "General@register_dosen_page")->name('ujianRegisterDosen');
        Route::post('/register/dosen', "General@register_dosen_process")->name('ujianRegisterDosenProcess');

        Route::get('/register/mahasiswa', "General@register_mahasiswa_page")->name('ujianRegisterMahasiswa');
        Route::post('/register/mahasiswa', "General@register_mahasiswa_process")->name('ujianRegisterMahasiswaProcess');

        Route::get('/login', "General@login_page")->name('ujianLogin');
        Route::post('/login', "General@login_process")->name('ujianLoginProcess');
    });

    Route::group(['middleware' => 'authLoginUjian'], function () {

        Route::get('/member/profile', "General@profile_edit")->name('profileEdit');
        Route::post('/member/profile', "General@profile_update")->name('profileUpdate');

        Route::get('/ujian/list', "Ujian@list")->name('ujianList');

        Route::get('/ujian/create', "Ujian@create")->name('ujianCreate');
        Route::post('/ujian/create/bidang/sub/list', "Ujian@bidang_sub")->name('ujianCreateBidangSub');
        Route::post('/ujian/insert', "Ujian@insert")->name('ujianInsert');


        Route::get('/ujian/delete/{id_ujian}', "Ujian@delete")->name('ujianDelete');
        Route::get('/ujian/paper/{id_ujian}', "Ujian@paper_page")->name('ujianPaper');
        Route::post('/ujian/paper/insert', "Ujian@paper_insert")->name('ujianPaperInsert');
        Route::get('/ujian/finish/{id_ujian}', "Ujian@finish")->name('ujianFinish');
        Route::get('/ujian/download/{id_ujian}', "Ujian@download")->name('ujianDownload');


        Route::get('/mahasiswa/list', "Mahasiswa@list")->name('mahasiswaList');
        Route::get('/mahasiswa/create', "Mahasiswa@create")->name('mahasiswaCreate');
        Route::post('/mahasiswa/insert', "Mahasiswa@insert")->name('mahasiswaInsert');
        Route::get('/mahasiswa/edit/{id_peserta}', "Mahasiswa@edit")->name('mahasiswaEdit');
        Route::post('/mahasiswa/update/{id_peserta}', "Mahasiswa@update")->name('mahasiswaUpdate');
        Route::get('/mahasiswa/delete/{id_peserta}', "Mahasiswa@delete")->name('mahasiswaDelete');

        Route::get('/ujian/logout', "General@logout_process")->name('ujianLogoutProcess');

    });
});


Route::group(['prefix' => 'admin'], function () {

    Route::group(['namespace' => 'ManajemenSoal', 'middleware' => 'authLogin'], function () {

        Route::get('/bidang', "Bidang@index")->name('bidangList');
        Route::post('/bidang', "Bidang@insert")->name('bidangInsert');
        Route::delete('/bidang/{id}', "Bidang@delete")->name('bidangDelete');
        Route::get('/bidang/{id}', "Bidang@edit_modal")->name('bidangEditModal');
        Route::post('/bidang/{id}', "Bidang@update")->name('bidangUpdate');

        Route::get('/bidang-sub', "BidangSub@index")->name('bidangSubList');
        Route::post('/bidang-sub', "BidangSub@insert")->name('bidangSubInsert');
        Route::delete('/bidang-sub/{id}', "BidangSub@delete")->name('bidangSubDelete');
        Route::get('/bidang-sub/{id}', "BidangSub@edit_modal")->name('bidangSubEditModal');
        Route::post('/bidang-sub/{id}', "BidangSub@update")->name('bidangSubUpdate');

        Route::get('/soal', "Soal@index")->name('soalList');
        Route::post('/soal/bidang/sub', "Soal@bidang_sub")->name('soalBidangSub');

        Route::post('/soal/kategori', 'Soal@kategori_insert')->name('soalKategoriInsert');
        Route::get('/soal/kategori/edit/{id_soal_kategori}', 'Soal@kategori_edit_modal')->name('soalKategoriEdit');
        Route::post('/soal/kategori/update', 'Soal@kategori_update')->name('soalKategoriUpdate');
        Route::get('/soal/kategori/delete/{id_bidang_sub}', 'Soal@kategori_delete')->name('soalKategoriDelete');

        Route::post('/soal/sesi', 'Soal@sesi_insert')->name('soalSectionInsert');
        Route::get('/soal/sesi/edit/{id_soal_section}', 'Soal@sesi_edit_modal')->name('soalSectionEdit');
        Route::post('/soal/sesi/update', 'Soal@sesi_update')->name('soalSectionUpdate');
        Route::get('/soal/sesi/delete/{id_bidang_sub}', 'Soal@sesi_delete')->name('soalSectionDelete');

        Route::post('/soal/item', 'Soal@soal_insert')->name('soalInsert');
        Route::get('/soal/item/edit/{id_soal}', 'Soal@soal_edit_modal')->name('soalEdit');
        Route::post('/soal/item/update', 'Soal@soal_update')->name('soalUpdate');
        Route::get('/soal/item/delete/{id_soal}', 'Soal@soal_delete')->name('soalDelete');

    });

    Route::namespace('General')->group(function () {

        Route::get('undercosntruction', "Underconstruction@index")->name('underconstructionPage');
        Route::post('city/list', "General@city_list")->name('cityList');
        Route::post('subdistrict/list', "General@subdistrict_list")->name('subdistrictList');

        Route::group(['middleware' => 'checkLogin'], function () {
            Route::get('/', "Login@index")->name("loginPage");
            Route::post('/roles/login', 'Login@roles')->name("rolesLogin");
            Route::post('/login', "Login@do")->name("loginDo");
        });

        Route::group(['middleware' => 'authLogin'], function () {
            Route::get('/dashboard', "Dashboard@index")->name('dashboardPage');
            Route::get('/whatsapp-test', "Dashboard@whatsapp_test")->name('whatsappTest');
            Route::get('/logout', "Logout@do")->name('logoutDo');
            Route::get('/profile', "Profile@index")->name('profilPage');
            Route::post('/profile/administrator', "Profile@update_administrator")->name('profilAdministratorUpdate');

            Route::post('/centak/pdf', "General@cetak_pdf")->name('cetakPdf');

        });
    });

// =============== Master Data =================== //

    Route::group(['namespace' => 'MasterData', 'middleware' => 'authLogin'], function () {

        // Satuan
        Route::get('/satuan', "Satuan@index")->name('satuanList');
        Route::post('/satuan', "Satuan@insert")->name('satuanInsert');
        Route::delete('/satuan/{id}', "Satuan@delete")->name('satuanDelete');
        Route::get('/satuan/{id}', "Satuan@edit_modal")->name('satuanEditModal');
        Route::post('/satuan/{id}', "Satuan@update")->name('satuanUpdate');

        // Jenis Barang
        Route::get('/jenis-barang', "JenisBarang@index")->name('jenisBarangList');
        Route::post('/jenis-barang', "JenisBarang@insert")->name('jenisBarangInsert');
        Route::delete('/jenis-barang/{id}', "JenisBarang@delete")->name('jenisBarangDelete');
        Route::get('/jenis-barang/{id}', "JenisBarang@edit_modal")->name('jenisBarangEditModal');
        Route::post('/jenis-barang/{id}', "JenisBarang@update")->name('jenisBarangUpdate');

        // Supplier
        Route::get('/supplier', "Supplier@index")->name('supplierList');
        Route::post('/supplier', "Supplier@insert")->name('supplierInsert');
        Route::delete('/supplier/{id}', "Supplier@delete")->name('supplierDelete');
        Route::get('/supplier/{id}', "Supplier@edit_modal")->name('supplierEditModal');
        Route::post('/supplier/{id}', "Supplier@update")->name('supplierUpdate');

        // Supplier
        Route::get('/pelanggan', "Pelanggan@index")->name('pelangganList');
        Route::post('pelanggan', "Pelanggan@insert")->name('pelangganInsert');
        Route::delete('/pelanggan/{id}', "Pelanggan@delete")->name('pelangganDelete');
        Route::get('/pelanggan/{id}', "Pelanggan@edit_modal")->name('pelangganEditModal');
        Route::post('/pelanggan/{id}', "Pelanggan@update")->name('pelangganUpdate');

        // Karyawan
        Route::get('/karyawan', "Karyawan@index")->name('karyawanPage');
        Route::post('/karyawan', "Karyawan@insert")->name('karyawanInsert');
        Route::get('/karyawan-list', "Karyawan@list")->name('karyawanList');
        Route::delete('/karyawan/{id}', "Karyawan@delete")->name('karyawanDelete');
        Route::get('/karyawan/{id}', "Karyawan@edit_modal")->name('karyawanEditModal');
        Route::post('/karyawan/{id}', "Karyawan@update")->name('karyawanUpdate');

        // User
        Route::get('/user', "User@index")->name('userPage');
        Route::post('/user', "User@insert")->name('userInsert');
        Route::get('/user-list', "User@list")->name('userList');
        Route::delete('/user/{kode}', "User@delete")->name('userDelete');
        Route::get('/user/modal/{kode}', "User@edit_modal")->name('userEditModal');
        Route::post('/user/{kode}', "User@update")->name('userUpdate');

        // Role User
        Route::get('/user-role', "UserRole@index")->name('userRolePage');
        Route::post('/user-role', "UserRole@insert")->name('userRoleInsert');
        Route::delete('/user-role/{id}', "UserRole@delete")->name('userRoleDelete');
        Route::get('/user-role/modal/{id}', "UserRole@edit")->name('userRoleEditModal');
        Route::post('/user-role/{id}', "UserRole@update")->name('userRoleUpdate');
        Route::get('/user-role/akses/{id}', "UserRole@akses")->name('userRoleAkses');
        Route::post('/user-role/akses/{id}', "UserRole@akses_update")->name('userRoleAksesUpdate');

        // Metode Tindakan
        Route::get('/metode-tindakan', "ActionMethod@index")->name('actionMethodPage');
        Route::post('/metode-tindakan', "ActionMethod@insert")->name('actionMethodInsert');
        Route::get('/metode-tindakan-list', "ActionMethod@list")->name('actionMethodList');
        Route::delete('/metode-tindakan/{kode}', "ActionMethod@delete")->name('actionMethodDelete');
        Route::get('/metode-tindakan/modal/{kode}', "ActionMethod@edit_modal")->name('actionMethodEditModal');
        Route::post('/metode-tindakan/{kode}', "ActionMethod@update")->name('actionMethodUpdate');

    });

});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    //Artisan::call('route:cache');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

