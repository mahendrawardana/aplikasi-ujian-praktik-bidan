<?php return array (
  'sans-serif' => array(
    'normal' => $fontDir . '/Helvetica',
    'bold' => $fontDir . '/Helvetica-Bold',
    'italic' => $fontDir . '/Helvetica-Oblique',
    'bold_italic' => $fontDir . '/Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $fontDir . '/Times-Roman',
    'bold' => $fontDir . '/Times-Bold',
    'italic' => $fontDir . '/Times-Italic',
    'bold_italic' => $fontDir . '/Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $fontDir . '/Times-Roman',
    'bold' => $fontDir . '/Times-Bold',
    'italic' => $fontDir . '/Times-Italic',
    'bold_italic' => $fontDir . '/Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $fontDir . '/Courier',
    'bold' => $fontDir . '/Courier-Bold',
    'italic' => $fontDir . '/Courier-Oblique',
    'bold_italic' => $fontDir . '/Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $fontDir . '/Helvetica',
    'bold' => $fontDir . '/Helvetica-Bold',
    'italic' => $fontDir . '/Helvetica-Oblique',
    'bold_italic' => $fontDir . '/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $fontDir . '/ZapfDingbats',
    'bold' => $fontDir . '/ZapfDingbats',
    'italic' => $fontDir . '/ZapfDingbats',
    'bold_italic' => $fontDir . '/ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $fontDir . '/Symbol',
    'bold' => $fontDir . '/Symbol',
    'italic' => $fontDir . '/Symbol',
    'bold_italic' => $fontDir . '/Symbol',
  ),
  'serif' => array(
    'normal' => $fontDir . '/Times-Roman',
    'bold' => $fontDir . '/Times-Bold',
    'italic' => $fontDir . '/Times-Italic',
    'bold_italic' => $fontDir . '/Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $fontDir . '/Courier',
    'bold' => $fontDir . '/Courier-Bold',
    'italic' => $fontDir . '/Courier-Oblique',
    'bold_italic' => $fontDir . '/Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $fontDir . '/Courier',
    'bold' => $fontDir . '/Courier-Bold',
    'italic' => $fontDir . '/Courier-Oblique',
    'bold_italic' => $fontDir . '/Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $fontDir . '/DejaVuSans-Bold',
    'bold_italic' => $fontDir . '/DejaVuSans-BoldOblique',
    'italic' => $fontDir . '/DejaVuSans-Oblique',
    'normal' => $fontDir . '/DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $fontDir . '/DejaVuSansMono-Bold',
    'bold_italic' => $fontDir . '/DejaVuSansMono-BoldOblique',
    'italic' => $fontDir . '/DejaVuSansMono-Oblique',
    'normal' => $fontDir . '/DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $fontDir . '/DejaVuSerif-Bold',
    'bold_italic' => $fontDir . '/DejaVuSerif-BoldItalic',
    'italic' => $fontDir . '/DejaVuSerif-Italic',
    'normal' => $fontDir . '/DejaVuSerif',
  ),
  'roboto' => array(
    'normal' => $fontDir . '/roboto-normal_0dac12f839c0bcdffbb9aebc77c41375',
  ),
  'summernote' => array(
    'normal' => 'C:\xampp\htdocs\ternak\storage\fonts//45a8659b48f03b8f912db1c48a34f987',
  ),
  'socicon' => array(
    'normal' => 'C:\xampp\htdocs\ternak\storage\fonts//5bfc0efd62ca4e66bd28fce2da20957e',
  ),
  'lineawesome' => array(
    'normal' => 'C:\xampp\htdocs\ternak\storage\fonts//25e75f8e830f645f61ab3c649d4020dc',
  ),
  'flaticon' => array(
    'normal' => 'C:\xampp\htdocs\ternak\storage\fonts//2c6f9cd5a9ee807d4bdfb33f5ec429fb',
  ),
  'metronic' => array(
    'normal' => 'C:\xampp\htdocs\ternak\storage\fonts//849de39981edc86d6ad2792884cbb9fe',
  ),
  'font awesome 5 brands' => array(
    'normal' => 'C:\xampp\htdocs\ternak\storage\fonts//a082288a3e46cba85f3452eba5cb8246',
  ),
  'font awesome 5 free' => array(
    'normal' => 'C:\xampp\htdocs\ternak\storage\fonts//4cb81350aa706980bc12d235865fa560',
  ),
  'open sans' => array(
    'normal' => 'C:\xampp\htdocs\ternak\storage\fonts//07ffedf4983a20afacaf57bfba0ebf45',
  ),
) ?>